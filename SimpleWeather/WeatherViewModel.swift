//
//  WeatherViewModel.swift
//  SimpleWeather
//
//  Created by Radu Petrisel on 21.02.2023.
//

import Foundation
import CoreLocation
import Combine
import SwiftUI

class WeatherViewModel: ObservableObject {
    @Published var currentTemperature: TemperatureViewModel = TemperatureViewModel(temperature: 0, unit: "")
    @Published var dayWeatherDataForWeek: [DailyWeatherViewModel] = []
    @Published var windViewModel: WindViewModel = WindViewModel(speed: 0, direction: 0)
    
    private let weatherDataManger = WeatherDataManager()
    private var dateFormatter: DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        df.timeZone = TimeZone.current
        return df
    }()
    
    @MainActor
    func updateData(at location: CLLocationCoordinate2D) async {
        let weatherData = await weatherDataManger.requestData(at: location)
        
        if let currentWeather = weatherData.currentWeather,
           let dailyUnits = weatherData.dailyUnits {
            currentTemperature = TemperatureViewModel(temperature: currentWeather.temperature, unit: dailyUnits.minimum)
        }
        
        if let dailyData = weatherData.daily,
           let dailyUnits = weatherData.dailyUnits{
            processDailyData(dailyData, temperatureUnit: dailyUnits.minimum)
        }
        
        if let currentWeather = weatherData.currentWeather {
            windViewModel = WindViewModel(speed: currentWeather.windspeed, direction: currentWeather.winddirection)
        }
    }
    
    func processDailyData(_ dailyData: DailyWeather, temperatureUnit unit: String) {
        guard dailyData.dates.count == 7 else { return }
        guard dailyData.minimums.count == 7 else { return }
        guard dailyData.maximums.count == 7 else { return }
        guard dailyData.weathercodes.count == 7 else { return }
        
        for index in 0..<7 {
            let date = dateFormatter.date(from: dailyData.dates[index])!
            let weatherCode = dailyData.weathercodes[index]
            let minimumTemperature = dailyData.minimums[index]
            let maximumTemperature = dailyData.maximums[index]
            
            let dayWeatherData = DailyWeatherViewModel(
                date: date,
                minimumTemperature: TemperatureViewModel(temperature: minimumTemperature, unit: unit),
                maximumTemperature: TemperatureViewModel(temperature: maximumTemperature, unit: unit),
                weathercode: weatherCode)
            
            dayWeatherDataForWeek.append(dayWeatherData)
        }
    }
}

struct WindViewModel {
    let speed: Float
    let direction: Angle
    
    init(speed: Float, direction: Float) {
        self.speed = speed
        self.direction = Angle(degrees: Double(direction))
    }
}

struct DailyWeatherViewModel: Identifiable {
    let id = UUID()
    let date: Date
    let minimumTemperature: TemperatureViewModel
    let maximumTemperature: TemperatureViewModel
    let weathercode: Int
}

struct TemperatureViewModel : CustomStringConvertible {
    let temperature: Float
    let unit: String
    
    var description: String {
        "\(roundTemperature(temperature))\(unit)"
    }
}

fileprivate func roundTemperature(_ temperature: Float) -> Int {
    return Int(temperature.rounded(.up))
}
