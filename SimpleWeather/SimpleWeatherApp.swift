//
//  SimpleWeatherApp.swift
//  SimpleWeather
//
//  Created by Radu Petrisel on 20.02.2023.
//

import SwiftUI

@main
struct SimpleWeatherApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
