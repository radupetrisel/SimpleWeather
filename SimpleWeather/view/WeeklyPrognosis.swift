//
//  WeeklyPrognosis.swift
//  SimpleWeather
//
//  Created by Radu Petrisel on 27.02.2023.
//

import SwiftUI

struct WeeklyPrognosis: View {
    let weeklyPrognosis: [DailyWeatherViewModel]
    
    var body: some View {
        List {
            ForEach(weeklyPrognosis, content: DailyWeatherRow.init)
        }
    }
}

struct WeeklyPrognosis_Previews: PreviewProvider {
    static var previews: some View {
        WeeklyPrognosis(weeklyPrognosis: [])
    }
}
