//
//  WindCompass.swift
//  SimpleWeather
//
//  Created by Radu Petrisel on 25.02.2023.
//

import SwiftUI

struct WindCompass: View {
    let windViewModel: WindViewModel
    let singleSize: CGFloat
    
    var body: some View {
        ZStack {
            Arc(startAngle: 0)
                .ofSize(singleSize)
            
            Arc(startAngle: 90)
                .ofSize(singleSize)
            
            Arc(startAngle: 180)
                .ofSize(singleSize)
            
            Arc(startAngle: 270)
                .ofSize(singleSize)
            
            Arrow()
                .stroke(Color.primary, lineWidth: 3)
                .padding(5)
                .rotationEffect(windViewModel.direction)
                .frame(width: singleSize, height: singleSize, alignment: .center)
            
            Text("N")
                .bold()
                .offset(x: 0, y: -singleSize / 2)
            
            Text("S")
                .bold()
                .offset(x: 0, y: singleSize / 2)
            
            Text("E")
                .bold()
                .offset(x: singleSize / 2, y: 0)
            
            Text("W")
                .bold()
                .offset(x: -singleSize / 2, y: 0)
            
            Text(String(format: "%.f", arguments: [windViewModel.speed]))
                .font(.largeTitle)
        }
    }
}

struct WindCompass_Previews: PreviewProvider {
    static var previews: some View {
        WindCompass(windViewModel: WindViewModel(speed: 12, direction: 120), singleSize: 200)
            .frame(width: 500, height: 500)
    }
}
