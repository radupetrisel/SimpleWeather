//
//  Arrow.swift
//  SimpleWeather
//
//  Created by Radu Petrisel on 26.02.2023.
//

import Foundation
import SwiftUI

struct Arrow : Shape {
    
    func path(in rect: CGRect) -> Path {
        var path = Path()
        
        let start = CGPoint(x: rect.minX, y: rect.midY)
        path.move(to: start)
        path.addLine(to: CGPoint(x: start.x + 10, y: start.y - 10))
        
        path.move(to: start)
        path.addLine(to: CGPoint(x: start.x + 10, y: start.y + 10))
        
        path.move(to: start)
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.midY))
        
        
        return path
    }
}
