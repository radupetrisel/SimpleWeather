//
//  TodayWeather.swift
//  SimpleWeather
//
//  Created by Radu Petrisel on 27.02.2023.
//

import SwiftUI

struct TodayWeather: View {
    let locationName: String
    @ObservedObject var weatherViewModel: WeatherViewModel
    
    var body: some View {
        Text(locationName)
            .font(.largeTitle)
            .bold()
        
        Text(weatherViewModel.currentTemperature.description)
            .font(.system(size: 50))
        
        if let todayMinMax = weatherViewModel.dayWeatherDataForWeek.first {
            VStack {
                Text(todayMinMax.weathercode.toWMOString())
                    .font(.system(size: 20))
                    .foregroundColor(.gray)
                
                HStack {
                    Text("Low: \(todayMinMax.minimumTemperature.description)")
                    Text("High: \(todayMinMax.maximumTemperature.description)")
                }
            }
        }
    }
}

struct TodayWeather_Previews: PreviewProvider {
    static var previews: some View {
        TodayWeather(locationName: "Location", weatherViewModel: WeatherViewModel())
    }
}
