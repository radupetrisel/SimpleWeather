//
//  DailyWeatherRow.swift
//  SimpleWeather
//
//  Created by Radu Petrisel on 25.02.2023.
//

import SwiftUI

struct DailyWeatherRow: View {
    let dailyWeatherViewModel: DailyWeatherViewModel
    
    var body: some View {
        HStack {
            Text(getPrettyDay(dailyWeatherViewModel.date))
                .frame(minWidth: 150, alignment: .leading)
            
            Spacer()
            
            Image(systemName: dailyWeatherViewModel.weathercode.toWeatherIconName())
            
            Spacer()
            
            Text(dailyWeatherViewModel.minimumTemperature.description)
                .foregroundColor(.gray)
                .frame(minWidth: 50)
            
            Spacer()
            
            Text(dailyWeatherViewModel.maximumTemperature.description)
                .frame(minWidth: 50)
        }
    }
    
    func getPrettyDay(_ date: Date) -> String {
        if Calendar.current.isDateInTomorrow(date) {
            return "Tomorrow"
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = .current
        dateFormatter.calendar = .current
        dateFormatter.dateFormat = "EEEE"
        
        return dateFormatter.string(from: date)
    }
}

struct DailyWeatherRow_Previews: PreviewProvider {
    static var previews: some View {
        DailyWeatherRow(dailyWeatherViewModel: DailyWeatherViewModel(date: Date.now, minimumTemperature: TemperatureViewModel(temperature: 0, unit: "C"), maximumTemperature: TemperatureViewModel(temperature: 10, unit: "C"), weathercode: 0))
    }
}
