//
//  Arc.swift
//  SimpleWeather
//
//  Created by Radu Petrisel on 26.02.2023.
//

import Foundation
import SwiftUI

struct Arc : Shape {
    let startAngle: Double
    
    func path(in rect: CGRect) -> Path {
        var path = Path()
        
        path.addArc(center: CGPoint(x: rect.midX, y: rect.midY), radius: rect.width / 2, startAngle: Angle(degrees: startAngle + 5), endAngle: Angle(degrees: startAngle + 85), clockwise: false)
        
        return path
    }
    
    func ofSize(_ size: Double) -> some View {
        self
            .stroke(Color.secondary, lineWidth: 5)
            .frame(width: size, height: size, alignment: .center)
    }
}
