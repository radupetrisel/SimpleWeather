//
//  ContentView.swift
//  SimpleWeather
//
//  Created by Radu Petrisel on 20.02.2023.
//

import SwiftUI

struct ContentView: View {
    @StateObject private var weatherViewModel = WeatherViewModel()
    @StateObject private var locationManagerDelegate = LocationManagerDelegate()
    
    var body: some View {
        VStack {
            
            TodayWeather(locationName: locationManagerDelegate.locationName, weatherViewModel: weatherViewModel)
            
            HStack {
                let weeklyPrognosisData = Array(weatherViewModel.dayWeatherDataForWeek.dropFirst())
                
                WeeklyPrognosis(weeklyPrognosis: weeklyPrognosisData)
                    .padding(.trailing)
                
                WindCompass(windViewModel: weatherViewModel.windViewModel, singleSize: 150)
            }
        }
        .task {
            await weatherViewModel.updateData(at: locationManagerDelegate.coordinates)
        }
        .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .frame(width: 600, height: 400)
    }
}
