//
//  LocationManager.swift
//  SimpleWeather
//
//  Created by Radu Petrisel on 20.02.2023.
//

import Foundation
import CoreLocation

class LocationManagerDelegate : NSObject, ObservableObject, CLLocationManagerDelegate {
    let manager = CLLocationManager()
    @Published var coordinates: CLLocationCoordinate2D = CLLocationCoordinate2D()
    @Published var locationName: String = ""
    
    override init() {
        super.init()
        
        manager.delegate = self
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            self.coordinates = location.coordinate
                
            let geocoder = CLGeocoder()
            
            geocoder.reverseGeocodeLocation(location) {
                [unowned self] placemarks, error in
                    if error == nil, let placemark = placemarks?.first {
                        self.locationName = placemark.locality ?? ""
                    }
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
    }
}
