//
//  WeatherCodeInterpretation.swift
//  SimpleWeather
//
//  Created by Radu Petrisel on 23.02.2023.
//

import Foundation

extension Int {
    
    func toWMOString() -> String {
        switch self {
        case 0:
            return "Clear sky"
        case 1:
            return "Mainly clear"
        case 2:
            return "Partly cloudy"
        case 3:
            return "Overcast"
        case 45:
            return "Fog"
        case 48:
            return "Depositing rime fog"
        case 51, 53, 55:
            return "Light drizzle"
        case 56, 57:
            return "Freezing drizzle"
        case 61, 63, 65:
            return "Rain"
        case 66, 67:
            return "Freezing rain"
        case 71, 73, 75:
            return "Snow"
        case 77:
            return "Snow grains"
        case 80, 81, 82:
            return "Showers"
        case 85, 86:
            return "Snow showers"
        case 95:
            return "Thunderstorm"
        case 96, 99:
            return "Thunderstorm with hail"
        default:
            return "--"
        }
    }
    
    func toWeatherIconName() -> String {
        switch self {
        case 0:
            return "sun.max.fill"
        case 1:
            return "sun.max"
        case 2:
            return "cloud.sun"
        case 3:
            return "cloud.fill"
        case 45:
            return "cloud.fog"
        case 48:
            return "cloud.fog.fill"
        case 51, 53, 55:
            return "cloud.drizzle"
        case 56, 57:
            return "cloud.drizzle.fi"
        case 61, 63, 65:
            return "cloud.rain"
        case 66, 67:
            return "cloud.heavyrain"
        case 71, 73, 75, 77:
            return "cloud.snow"
        case 80, 81, 82:
            return "cloud.rain"
        case 85, 86:
            return "cloud.snow"
        case 95, 96, 99:
            return "cloud.bolt.rain"
        default:
            return "sun.max"
        }
    }
}
