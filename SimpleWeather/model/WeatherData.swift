//
//  WeatherData.swift
//  SimpleWeather
//
//  Created by Radu Petrisel on 21.02.2023.
//

import Foundation

struct WeatherData : Codable {
    enum CodingKeys: String, CodingKey {
        case daily
        case currentWeather = "current_weather"
        case dailyUnits = "daily_units"
    }
    
    static var empty: WeatherData {
        WeatherData()
    }
    
    var daily: DailyWeather?
    var currentWeather: CurrentWeather?
    var dailyUnits: DailyUnits?
}

struct CurrentWeather: Codable {
    var temperature: Float = 0
    var time: String = ""
    var windspeed: Float = 0
    var winddirection: Float = 0
}

struct DailyWeather: Codable {
    enum CodingKeys: String, CodingKey {
        case weathercodes = "weathercode"
        case dates = "time"
        case minimums = "temperature_2m_min"
        case maximums = "temperature_2m_max"
    }
    
    var weathercodes: [Int] = []
    var minimums: [Float] = []
    var maximums: [Float] = []
    var dates: [String] = []
}

struct DailyUnits: Codable {
    enum CodingKeys: String, CodingKey {
        case timeFormat = "time"
        case minimum = "temperature_2m_min"
        case maximum = "temperature_2m_max"
    }
    
    var timeFormat: String
    var minimum: String
    var maximum: String
}
