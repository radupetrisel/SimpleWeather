//
//  WeatherDataManager.swift
//  SimpleWeather
//
//  Created by Radu Petrisel on 21.02.2023.
//

import Foundation
import CoreLocation

struct WeatherDataManager {
    
    func requestData(at location: CLLocationCoordinate2D) async -> WeatherData {
        guard let url = OpenMeteoUrlBuilder(at: location)
            .withCurrentWeather()
            .withDailyWeather(.maximumTemperature, .minimumTemperature, .weathercode)
            .build()
        else {
            return WeatherData.empty
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        do {
            let (data, _) = try await URLSession.shared.data(for: urlRequest)
            
            return try JSONDecoder().decode(WeatherData.self, from: data)
        } catch {
            return WeatherData.empty
        }
    }
}

/*
 https://api.open-meteo.com/v1/forecast?latitude=52.52&longitude=13.41&current_weather=true&timezone=Europe%2FMoscow
 */
