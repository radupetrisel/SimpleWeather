//
//  OpenMeteoUrlBuilder.swift
//  SimpleWeather
//
//  Created by Radu Petrisel on 25.02.2023.
//

import Foundation
import CoreLocation

class OpenMeteoUrlBuilder {
    private static let CURRENT_WEATHER = "current_weather"
    private static let TIMEZONE = "timezone"
    private static let DAIlY = "daily"
    
    private var urlComponents: URLComponents
    private var queryItems: [URLQueryItem]
    
    init(at cooridnates: CLLocationCoordinate2D) {
        urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "api.open-meteo.com"
        urlComponents.path = "/v1/forecast"
        queryItems = [
            URLQueryItem(name: "latitude", value: String(format: "%.2f", arguments: [cooridnates.latitude])),
            URLQueryItem(name: "longitude", value: String(format: "%.2f", arguments: [cooridnates.longitude]))
        ]
    }
    
    func withCurrentWeather() -> OpenMeteoUrlBuilder {
        let timeZoneId = TimeZone.current.identifier
        queryItems.append(URLQueryItem(name: OpenMeteoUrlBuilder.CURRENT_WEATHER, value: true.description))
        queryItems.append(URLQueryItem(name: OpenMeteoUrlBuilder.TIMEZONE, value: timeZoneId))
        return self
    }
    
    func withDailyWeather(_ params: WeatherDataParam...) -> OpenMeteoUrlBuilder {
        let paramsString = params.map { $0.rawValue }.joined(separator: ",")
        queryItems.append(URLQueryItem(name: OpenMeteoUrlBuilder.DAIlY, value: paramsString))
        return self
    }
    
    func build() -> URL? {
        urlComponents.queryItems = queryItems
        urlComponents.percentEncodedQuery = urlComponents.percentEncodedQuery?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        return urlComponents.url
    }
}

enum WeatherDataParam: String {
    case minimumTemperature = "temperature_2m_min"
    case maximumTemperature = "temperature_2m_max"
    case weathercode = "weathercode"
}
